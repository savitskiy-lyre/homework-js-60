import React from 'react';

const FormPost = ({onMessageSubmit, onChangePostInp, postInp}) => {
   return (
     <form className="sendMessage my-2" onSubmit={onMessageSubmit}>
        <input className="form-control" value={postInp} onChange={onChangePostInp} type="text"
               placeholder="What's Happening ?" required/>
        <button className="btn btn-primary ms-2">Send</button>
     </form>
   );
};

export default FormPost;